from sage.all import *
#def whittaker_W(k, m, z):
#    return exp(-z/2.) * z **(m+0.5) * hypergeometric_U(1/2+m-k, 1+2*m, z)
#def whittaker_M(k, m, z):
#    return exp(-z/2.) * z **(m+1/2) * hypergeometric([1/2+m-k], [1+2*m], z)
#
#def log_w_prime(k, m, z):
#     return (1/2-(m-1/2)/z) - 1/sqrt(z) * whittaker_W(k+1/2,m-1/2,z) / whittaker_W(k,m,z)
#
#print("log(w)' = ", log_w_prime(1.1474488447893996+2.39352191674488477E-004 * I, 0.5, 6065.8287682093933 -1.2653020799890684* I))

#val1 = whittaker_W(-I, 200.5, -500.*I)
#print("M(I, 200.5, -500I)", whittaker_M(k, m, z))


#def spherical_y(n,z):
#   return sqrt(pi/(2*z))*bessel_Y(n+1/2, z)

#print(spherical_y(13, (1+1.3*I)))
#p_10_4 = diff(legendre_P(10,x),x,4)*(1-x**2)**(2)
#p_10_4(1.+2*I)

# check for compute_v_dot_P

r = vector([1,0,0])
s = vector([1,1,0]) / sqrt(2)
N = 10
k = 0.3 + 0.75 * I
def sum_part(N, r, s, k):
    n_r = norm(r)
    n_s = norm(s)
    c_θ = r.inner_product(s)/(n_r*n_s)
    j = spherical_bessel_J
    V = [j(l,k*n_r) * j(l,k*n_s) for l in range(N)]
    return  (k/(4*π)* sum([legendre_P(l, c_θ) * (2*l+1) * V[l] for l in range(N) ])), V

sin_card = sin(k * norm(r-s))/(4*π*norm(r-s))
s_n, V = sum_part(N, r, s, k)
numerical_approx(s_n-sin_card)
