module m_hypergeom

    use iso_c_binding

    implicit none
    public :: whittaker_w, whittaker_m, compute_u, robust_gamma
    public :: log_w_prime
    public :: bessel_j, bessel_y, spherical_bessel_j
    public :: spherical_y

    interface
        subroutine compute_gamma(z_x, z_y, u_x, u_y) bind(C, name="compute_gamma")
            use iso_c_binding
            real(c_double), value :: z_x, z_y
            real(c_double) :: u_x, u_y
        end subroutine compute_gamma

        subroutine compute_u(a, b, z_x, z_y, u_x, u_y) bind(C, name="compute_U")
            use iso_c_binding
            real(c_double), value :: a, b, z_x, z_y
            real(c_double) :: u_x, u_y
        end subroutine compute_u

        subroutine compute_whittaker_w_call_2D(k_x, k_y, m_x, m_y , z_x, z_y, u_x, u_y, asymp) & 
                   bind(C, name="compute_whittaker_w")
            use iso_c_binding
            real(c_double), value  :: k_x, k_y, m_x, m_y, z_x, z_y
            real(c_double):: u_x, u_y
            integer(c_int), value :: asymp
        end subroutine compute_whittaker_w_call_2D

        subroutine compute_log_w_prime_call_2D(k_x, k_y, m_x, m_y , z_x, z_y, u_x, u_y, use_formula) &
                   bind(C, name="compute_log_w_prime")
            use iso_c_binding
            real(c_double), value  :: k_x, k_y, m_x, m_y, z_x, z_y
            real(c_double):: u_x, u_y
            integer(c_int) :: use_formula
        end subroutine compute_log_w_prime_call_2D

        subroutine compute_whittaker_m_call_2D(k_x, k_y, m_x, m_y , z_x, z_y, u_x, u_y) & 
                   bind(C, name="compute_whittaker_m")
            use iso_c_binding
            real(c_double), value  :: k_x, k_y, m_x, m_y, z_x, z_y
            real(c_double):: u_x, u_y
        end subroutine compute_whittaker_m_call_2D

        subroutine compute_bessel_j(nu_x, nu_y, z_x, z_y, b_x, b_y) bind(C, name="compute_bessel_j")
            use iso_c_binding
            real(c_double), value  :: nu_x, nu_y, z_x, z_y
            real(c_double):: b_x, b_y
        end subroutine compute_bessel_j

        subroutine compute_bessel_y(nu_x, nu_y, z_x, z_y, b_x, b_y) bind(C, name="compute_bessel_y")
            use iso_c_binding
            real(c_double), value  :: nu_x, nu_y, z_x, z_y
            real(c_double):: b_x, b_y
        end subroutine compute_bessel_y

        subroutine compute_spherical_bessel_j(n, z_x, z_y, b_x, b_y) & 
                   bind(C, name="compute_spherical_bessel_j")
            use iso_c_binding
            integer(c_int), value  :: n 
            real(c_double), value  ::  z_x, z_y
            real(c_double):: b_x, b_y
        end subroutine compute_spherical_bessel_j

        subroutine compute_spherical_y(n, m, theta_x, theta_y, phi_x, phi_y, b_x, b_y) & 
                  bind(C,name="compute_spherical_y")
            use iso_c_binding
            integer(c_int), value  :: n ,m
            real(c_double), value  ::  theta_x, theta_y, phi_x, phi_y
            real(c_double):: b_x, b_y
        end subroutine compute_spherical_y

        subroutine compute_legendre_p(m_x, m_y, n_x, n_y, z_x, z_y, b_x, b_y) & 
                  bind(C,name="compute_legendre_p")
            use iso_c_binding
            real(c_double), value  :: m_x, m_y, n_x, n_y, z_x, z_y
            real(c_double) :: b_x, b_y
        end subroutine compute_legendre_p


        subroutine compute_legendre_p_rec(n, r, res) bind(C,name="compute_legendre_p_rec")
            use iso_c_binding
            integer(c_int), value :: n
            real(c_double), value :: r
            real(c_double) :: res
        end subroutine compute_legendre_p_rec
        
        subroutine compute_v_dot_p(n, v_x, v_y, theta, res_x, res_y) bind(C,name="compute_v_dot_P")
            use iso_c_binding
            integer(c_int), value :: n
            real(c_double) :: v_x(n+1), v_y(n+1)
            real(c_double) :: res_x, res_y
            real(c_double), value :: theta
        end subroutine compute_v_dot_p


    end interface

 contains

    complex(c_double) function robust_gamma(z) result(res)
        complex(c_double), intent(in) :: z

        call compute_gamma( z % re, z % im, res % re, res % im)
  
    end function robust_gamma

    complex(c_double) function whittaker_w(k, m, z) result(res)
        complex(c_double), intent(in) :: k, m, z

        call compute_whittaker_w_call_2D( k%re, k%im, m%re, m%im, z%re, z%im, res%re, res%im, 0)
  
    end function whittaker_w


    complex(c_double) function log_w_prime(k, m, z) result(res)
        complex(c_double), intent(in) :: k, m, z

        call compute_log_w_prime_call_2D( k%re, k%im, m%re, m%im, z%re, z%im, res%re, res%im, 2)
  
    end function log_w_prime


    complex(c_double) function whittaker_m(k, m, z) result(res)
        complex(c_double), intent(in) :: k, m, z

        call compute_whittaker_m_call_2D( k%re, k%im, m%re, m%im, z%re, z%im, res%re, res%im)
  
    end function whittaker_m


    complex(c_double) function bessel_j(nu, z) result(res)
        complex(c_double), intent(in) :: nu, z

        call compute_bessel_j(nu%re, nu%im, z%re, z%im, res%re, res%im)
  
    end function bessel_j

    ! complex interface for spherical_bessel_j
    complex(c_double) function spherical_bessel_j(n, z) result(res)
        integer(c_int), intent(in) :: n
        complex(c_double), intent(in) ::  z

        call compute_spherical_bessel_j(n, z%re, z%im, res%re, res%im)
  
    end function spherical_bessel_j
    ! complex interface for spherical_y
    complex(c_double) function spherical_y(n, m,  theta, phi) result(res)
        integer(c_int), intent(in) :: n,m
        complex(c_double), intent(in) ::  theta, phi

        call compute_spherical_y(n, m, theta%re, theta%im, phi%re, phi%im, res%re, res%im)
  
    end function spherical_y

    ! complex interface for bessel_y
    complex(c_double) function bessel_y(nu, z) result(res)
        complex(c_double), intent(in) :: nu, z

        call compute_bessel_y(nu%re, nu%im, z%re, z%im, res%re, res%im)
  
    end function bessel_y

    ! complex interface for legendre_p
    complex(c_double) function legendre_p( m, n, z ) result(res)
        complex(c_double), intent(in) :: m, n, z

        call compute_legendre_p(m%re, m%im, n%re, n%im, z%re, z%im, res%re, res%im)
  
    end function legendre_p

   ! function interface for legendre_p_rec
    real(c_double) function legendre_p_rec( n, r) result(res)
        integer(c_int), intent(in) :: n 
        real(c_double), intent(in) :: r 

        call compute_legendre_p_rec(n, r, res)
  
    end function legendre_p_rec

    ! function interface for legendre_p_rec
    complex(c_double) function v_dot_P(v,theta) result(res)
        complex(c_double), intent(in) :: v(:)
        real(c_double), intent(in) :: theta
        integer(c_int) :: n_v

        n_v = size(v, 1) - 1
        call compute_v_dot_p(n_v, real(v, c_double) ,aimag(v), theta, res%re, res%im)
  
    end function v_dot_P


end module m_hypergeom
