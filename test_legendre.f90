module test_legendre

    use iso_c_binding
    use m_asserts
    use m_hypergeom
    implicit none

    real(c_double), parameter :: prec = real(1.e-12, c_double)
    real(c_double) :: res_x, res_x_check, r
    complex(c_double) :: res,  res_check
contains   
    
    subroutine test_legendre_p
        res_check = cmplx( 1.84792608000000d9, -3.55314960000000d9, c_double)
        res = legendre_p( dcmplx(10.d0, 0.d0), dcmplx(4.d0, 0.d0), dcmplx( 1.d0, 2.d0) )
        call assert_equals_c( res, res_check, prec )
    end subroutine test_legendre_p


    subroutine test_legendre_p_rec
        res_x_check = 0.0412029868641242d0
        res_x = legendre_p_rec( 25, 0.57d0 )
        call assert_equals_d( res_x, res_x_check, prec )
    end subroutine test_legendre_p_rec


    subroutine test_v_dot_p
        real(c_double) :: theta = acos(0.5d0*sqrt(2.d0))
        real(c_double), parameter :: pi = 4.d0*atan(1.d0) 
        complex(c_double) :: k =  complex(0.3d0, 0.75d0)
        complex(c_double), allocatable :: v(:)
        v = [1.1578202807271805d0, -0.05259817580430567d0, -3.0438470369992775d-05, 1.8333345105266042d-05, &
            -2.1154860578343941d-07, 8.696361041685358d-10, -5.063829767457681d-13, -7.536878726934683d-15, &
             2.7952124548363804d-17, -4.258088456568249d-20] + complex(0.d0, 1.d0) * &
            [-0.1695696988566226d0, 0.05985194868316748d0, -0.002024102751901475d0, 1.9211818941768738d-05, &
             -1.2021933707160461d-08, -7.29520445392943d-10, 4.331908711753989d-12, -1.0098923045231365d-14, & 
              4.8392479241003850d-18, 2.8346343221479610d-20]
                  
        res = v_dot_P(v, theta)
        res = res * k / (4.d0 * pi)
        res_check = complex(0.0276709663803235d0, 0.0613613553419072d0)
        call assert_equals_c( res, res_check, prec )
    end subroutine test_v_dot_p


end module test_legendre
