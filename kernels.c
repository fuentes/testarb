#include "acb.h"
#include "acb_hypgeom.h"
#include "arb_hypgeom.h"
#include "arb.h"
#include "arf.h"


#define PREC_MAX 4096
#define PREC_MIN 64
#define N_ASYMP 20



void compute_gamma(const double z_x, const double z_y, double * res_x, double *res_y)
{
    slong prec;

    acb_t z, res;
    arb_t res_re, res_imag;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(z);
    acb_init(res);
   
    arb_init(res_re);
    arb_init(res_imag);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y */
    acb_set_d_d(z, z_x, z_y);
    
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )

    {
        /* computes the U(m, n, z) function */
        acb_gamma(res, z, prec);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("PREC_MAX is reached while computing Gamma , the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_re); 
    arb_clear(res_imag);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
    flint_cleanup();
}

extern void compute_legendre_p(const double m_x, const double m_y,
                               const double n_x, const double n_y,
                               const double z_x, const double z_y,
                               double *res_x, double *res_y)

{
    slong prec;

    acb_t res, m, n, z;
    arb_t res_re, res_imag;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(z);
    acb_init(m);
    acb_init(n);
    acb_init(res);
   
    arb_init(res_re);
    arb_init(res_imag);

    arf_init(tmp_x);
    arf_init(tmp_y);

    acb_set_d_d(m, m_x, m_y);
    acb_set_d_d(n, n_x, n_y);
    acb_set_d_d(z, z_x, z_y);
    
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        int type = 0; 
        /* computes the  function */
        acb_hypgeom_legendre_p(res, m, n, z, type, prec);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("PREC_MAX is reached while computing legendre_p, the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(z);
    acb_clear(m);
    acb_clear(n);
    acb_clear(res);

    arb_clear(res_re); 
    arb_clear(res_imag);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
    flint_cleanup();
}

extern void compute_legendre_p_rec(const int n_i, const double r_d, double *res_d)
{
    slong prec;

    arb_t res, res_p, r;
    arf_t tmp;
    ulong n = (ulong) n_i;

    /* initialisation */
    arb_init(r);
    arb_init(res);
    arb_init(res_p);
   
    arf_init(tmp);

    arb_zero(res_p);
    arb_set_d(r, r_d);
    
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        arb_hypgeom_legendre_p_ui(res, res_p, n, r, prec);
        if (arb_is_finite(res) && arb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("PREC_MAX is reached while computing legendre_p_rec , the result is probably false");
    }

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp, res, prec);

    /* rounding to double */
    *res_d = arf_get_d(tmp, ARF_RND_NEAR);

    /* cleaning */  
    arb_clear(r);
    arb_clear(res);
    arb_clear(res_p);

    arf_clear(tmp);
    flint_cleanup();
}

/* 
 * computes the sum (res_x + i*res_y) = \sum_{l=0}^{n_v} (v_x[l] + i v_y[l]) * (2*l+1) * P_l(cos(theta))
 * where P_l is the Legendre polynomial
 * 
 * Input parameters : 
 * -----------------
 * v_x is a vector of reals of size n_v + 1
 * v_y is a vector of reals of size n_v + 1
 * theta is a double 
 * Ouput parameter :
 * ----------------
 * res_x is a double
 * res_y is a double
 */


extern void compute_v_dot_P(
  const int n_v, 
  const double * v_x, 
  const double * v_y, 
  const double theta,
  double * res_x,
  double * res_y)
{
    slong prec;

    acb_t pk1,
          pk2,
          w,
          v,
          z, 
          res;
    arf_t tmp_x, tmp_y;
    arb_t res_re, res_imag;
    int k;
    ulong n = (ulong) n_v;
    if (n < 2) {
      printf ("error in compute_v_dot_P : n must be greater or equal to 2");
      exit (-1);
    }

    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
	{

	  acb_init(pk1);
	  acb_init(pk2);
	  acb_init(w);
      acb_init(v);
	  acb_init(z);
      acb_init(res);

      arf_init(tmp_x);
      arf_init(tmp_y);
    
      arb_init(res_re);
      arb_init(res_imag);
      
      // P_0 <- 1
	  acb_one(pk2);

      // P_1 <- cos(theta)
      acb_set_d(z, cos(theta));
      acb_set(pk1, z);
    
      // res <- v[0] * 1 * P_0
      acb_zero(res);
      acb_set_d_d(v, v_x[0], v_y[0]);
      acb_addmul(res, pk2, v, prec);
      
      // res <- res + v[1] * 3 * P_1
      acb_set_d_d(v, v_x[1], v_y[1]);
      acb_mul_ui(v, v, 3, prec);
      acb_addmul(res, pk1, v, prec);


	  for (k = 2; k <= n; k++)
	  {
        /* pk = ((2k-1) z p_{k-1} - (k-1) p_{k-2} )/ k */
		acb_mul(w, pk1, z, prec);
		acb_mul_ui(w, w, 2 * k - 1, prec);
		acb_submul_ui(w, pk2, k - 1, prec);
		acb_div_ui(pk2, w, k, prec);
        /* pk2 nows contains pk */
 
        acb_set_d_d(v, v_x[k], v_y[k]);
        acb_mul_ui(v, v, 2 * k + 1, prec);
        acb_addmul(res, pk2, v, prec);
		acb_swap(pk1, pk2);
	  }

	  if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
		break;
	}
    if (prec > PREC_MAX)
    {
        printf("PREC_MAX is reached while computing legendre_p_rec , the result is probably false");
    }
    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    arb_clear(res_re);
    arb_clear(res_imag);
    arf_clear(tmp_x);
    arf_clear(tmp_y);
    acb_clear(res);
    acb_clear(pk1);
    acb_clear(pk2);
    acb_clear(v);
    acb_clear(w);
    acb_clear(z);
    flint_cleanup();
}

extern void compute_spherical_y(const int m, const int n, 
                                const double theta_x, const double theta_y,
                                const double phi_x, const double phi_y,
                                double *res_x, double *res_y)
{
    slong prec;

    acb_t res, phi, theta;
    arb_t res_re, res_imag;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(theta);
    acb_init(phi);
    acb_init(res);
   
    arb_init(res_re);
    arb_init(res_imag);

    arf_init(tmp_x);
    arf_init(tmp_y);

    acb_set_d_d(theta, theta_x, theta_y);
    acb_set_d_d(phi, phi_x, phi_y);
    
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        /* computes the Y_m^n(Θ,Φ) function */
        acb_hypgeom_spherical_y(res, (slong)m, (slong)n, theta, phi , prec);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("PREC_MAX is reached while computing Gamma , the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(theta);
    acb_clear(phi);
    acb_clear(res);

    arb_clear(res_re); 
    arb_clear(res_imag);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
    flint_cleanup();
}

void compute_U(const double m, const double n, const double z_x, const double z_y, double * res_x, double *res_y)
{
    slong prec = PREC_MIN;

    acb_t m_c, n_c, z, res;
    arb_t res_re, res_imag;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(m_c); 
    acb_init(n_c);
    acb_init(z);
    acb_init(res);
   
    arb_init(res_re);
    arb_init(res_imag);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * m_c <- m + i * 0
     * n_c <- n + i * 0
     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(m_c, m, 0.);
    acb_set_d_d(n_c, n, 0.);
    
    /* computes the U(m, n, z) function */
    acb_hypgeom_u(res, m_c, n_c, z, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(m_c); 
    acb_clear(n_c);
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_re); 
    arb_clear(res_imag);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
    flint_cleanup();
}

void compute_bessel_y(const double nu_x, const double nu_y, 
                      const double z_x, const double z_y, 
                      double * res_x, double * res_y)
{

    slong prec = PREC_MIN;
    acb_t nu, z, res;
    arb_t res_imag, res_re;
    arf_t tmp_x, tmp_y;
    /* initialisation */
    acb_init(nu); 
    acb_init(z);
    acb_init(res);

    arb_init(res_imag);
    arb_init(res_re);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* 
     * z <- z_x + i * z_y
     * nu <- nu_x + i * nu_y
     */
    acb_set_d_d(nu, nu_x, nu_y);
    acb_set_d_d(z, z_x, z_y);

    acb_hypgeom_bessel_y(res, nu, z, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(nu); 
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_imag);
    arb_clear(res_re);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

void compute_bessel_j(const double nu_x, const double nu_y, 
                      const double z_x, const double z_y, 
                      double * res_x, double * res_y)
{

    slong prec = PREC_MIN;
    acb_t nu, z, res;
    arb_t res_imag, res_re;
    arf_t tmp_x, tmp_y;
    /* initialisation */
    acb_init(nu); 
    acb_init(z);
    acb_init(res);

    arb_init(res_imag);
    arb_init(res_re);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* 
     * z <- z_x + i * z_y
     * nu <- nu_x + i * nu_y
     */
    acb_set_d_d(nu, nu_x, nu_y);
    acb_set_d_d(z, z_x, z_y);

    acb_hypgeom_bessel_j(res, nu, z, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(res_re, res);
    acb_get_imag(res_imag, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, res_re, prec);
    arb_get_ubound_arf(tmp_y, res_imag, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);

    /* cleaning */  
    acb_clear(nu); 
    acb_clear(z);
    acb_clear(res);

    arb_clear(res_imag);
    arb_clear(res_re);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

static void whittaker_m_with_prec(const double k_x, const double k_y, 
                                  const double m_x, const double m_y,
                                  const double z_x, const double z_y, 
                                  acb_t *res, slong prec)
{

    acb_t k, m, z, t, u, v, a, b, two;
    arb_t half;
    arf_t tmp_x, tmp_y;
    /* use M(a,b,z)=\frac{_1F_1(a,b,z)}{Γ(b)} */
    int regularized = 1; 

    /* initialisation */
    acb_init(k); 
    acb_init(m);
    acb_init(z);
    acb_init(*res);
    acb_init(t);
    acb_init(u);
    acb_init(v);
    acb_init(a);
    
    acb_init(b);
    acb_init(two);
 
    arb_init(half);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * k <- k_x + i * k_y
     * m <- m_x + i * m_y
     * half = 1/2
     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(k, k_x, k_y);
    acb_set_d_d(m, m_x, m_y);
    acb_set_d_d(two, 2., 0.);
    arb_set_d(half, 0.5);

    /* u = m + 1/2 */
    acb_add_arb(u, m, half, prec);  
     
    /* a = 1/2 + m - k */
    acb_sub(a, u, k, prec);

    /* b = 1 + 2m*/
    acb_one(b);
    acb_addmul(b, two, m, prec);

    /* computes the U(a, b, z) function */
    acb_hypgeom_m(*res, a, b, z, regularized, prec);

    /* computes t = z^{m+1/2}$ */
    acb_pow(t, z, u, prec);

    /* computes  u = exp(-z/2) */
    acb_neg(a, z);
    acb_mul_arb(v, a, half, prec);
    acb_exp(u, v, prec);

    /*  v = exp(-z/2) * z^{m+1/2} */
    acb_mul(v, u, t, prec);

    /* t  = exp(z/2) * z^{m+1/2} * U(a, b, z)*/
    acb_mul(t, v, *res, prec);

    acb_set(*res, t);

    /* cleaning */  
    acb_clear(m); 
    acb_clear(k);
    acb_clear(z);
    acb_clear(t);
    acb_clear(u);
    acb_clear(v);
    acb_clear(a);
    acb_clear(b);
    acb_clear(two);

    arb_clear(half);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

/* 
 * computes the whittaker function M_{k,m}(z) = exp(-z/2) * z^{m+1/2} * M(1/2 +m - k , 1 + 2m, z) 
 * where M(a,b,z) is the * Confluent Hypergeometric Function of the First Kind normalized by г(b)
 * 
 * Input parameters : 
 * -----------------
 * k = k_x + i * k_y
 * m = m_x + i * m_y
 * z = z_x + i * z_y
 * Ouput parameter :
 * ----------------
 * M_{k,m}(z) = res_x + i * res_y
 *
 */

void compute_whittaker_m(const double k_x, const double k_y, 
                        const double m_x, const double m_y,
                        const double z_x, const double z_y, 
                        double * res_x, double *res_y)
{
    slong prec;
    acb_t res;
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(res);

    arb_init(tmp1_x);
    arb_init(tmp1_y);

    arf_init(tmp2_x);
    arf_init(tmp2_y);

    acb_init(res);
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        whittaker_m_with_prec(k_x, k_y, m_x, m_y, z_x, z_y, &res, prec);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("WARNING PREC_MAX is reached, the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(tmp1_x, res);
    acb_get_imag(tmp1_y, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(res);
    
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);
    flint_cleanup();
}


static void whittaker_w_with_prec(const double k_x, const double k_y, 
                       const double m_x, const double m_y,
                       const double z_x, const double z_y, 
                       acb_t *res, slong prec,  const int asymp)
{

    acb_t k, m, z, t, u, v, a, b, two;
    arb_t half;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(k); 
    acb_init(m);
    acb_init(z);
    acb_init(*res);
    acb_init(t);
    acb_init(u);
    acb_init(v);
    acb_init(a);
    acb_init(b);
    acb_init(two);
 
    arb_init(half);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * k <- k_x + i * k_y
     * m <- m_x + i * m_y
     * half = 1/2
     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(k, k_x, k_y);
    acb_set_d_d(m, m_x, m_y);
    acb_set_d_d(two, 2., 0.);
    arb_set_d(half, 0.5);

    /* u = m + 1/2 */
    acb_add_arb(u, m, half, prec);  
     
    /* a = 1/2 + m - k */
    acb_sub(a, u, k, prec);

    /* b = 1 + 2m*/
    acb_one(b);
    acb_addmul(b, two, m, prec);

    /* computes the U(a, b, z) function */
    if (asymp == 0) 
    {
        acb_hypgeom_u(*res, a, b, z, prec);
    }
    else
    {
       if (acb_hypgeom_u_use_asymp(z, prec))
       {
           printf("WARNING : we use asymptotical but it does not seems to be necessary\n");
       }
       acb_hypgeom_u_asymp(*res, a, b, z, N_ASYMP, prec); /* Kontuz : normally it computes U*(a,b, z) = z^{a} U(a,b,z) */
    }


    /* computes t = z^{m+1/2}$ */
    acb_pow(t, z, u, prec);

    /* computes  u = exp(-z/2) */
    acb_neg(a, z);
    acb_mul_arb(v, a, half, prec);
    acb_exp(u, v, prec);

    /*  v = exp(-z/2) * z^{m+1/2} */
    acb_mul(v, u, t, prec);

    /* t  = exp(z/2) * z^{m+1/2} * U(a, b, z)*/
    acb_mul(t, v, *res, prec);

    acb_set(*res, t);

    /* cleaning */  
    acb_clear(m); 
    acb_clear(k);
    acb_clear(z);
    acb_clear(t);
    acb_clear(u);
    acb_clear(v);
    acb_clear(a);
    acb_clear(b);
    acb_clear(two);

    arb_clear(half);

    arf_clear(tmp_x);
    arf_clear(tmp_y);
}

/* 
 * computes the whittaker functions W_{k,m}(z) = exp(-z/2) * z^{m+1/2} * U(1/2 +m - k , 1 + 2m, z) 
 * where U is the * Confluent Hypergeometric Function of the Second Kind
 * [http://mathworld.wolfram.com/ConfluentHypergeometricFunctionoftheSecondKind.html]
 * 
 * Input parameters : 
 * -----------------
 * k = k_x + i * k_y
 * m = m_x + i * m_y
 * z = z_x + i * z_y
 * asymp : int (representing a boolean) to use or not asymptotical evaluation of U
 * Ouput parameter :
 * ----------------
 * W_{k,m}(z) = res_x + i * res_y
 *
 */

void compute_whittaker_w(const double k_x, const double k_y, 
                        const double m_x, const double m_y,
                        const double z_x, const double z_y, 
                        double * res_x, double *res_y, 
                        const int asymp)
{
    slong prec;
    acb_t res;
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(res);

    arb_init(tmp1_x);
    arb_init(tmp1_y);

    arf_init(tmp2_x);
    arf_init(tmp2_y);

    acb_init(res);
    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        whittaker_w_with_prec(k_x, k_y, m_x, m_y, z_x, z_y, &res, prec, asymp);
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("WARNING PREC_MAX is reached, the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(tmp1_x, res);
    acb_get_imag(tmp1_y, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(res);
    
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);
    flint_cleanup();
}

/* 
 * computes the whittaker ratio W'_{k,m}(z) / W{k,m}(z) = (1/2 - (m-1/2)/z) - 1/sqrt(z) * W_{k+1/2,m-1/2}(z)/W_{k,m}(z)
 * computes the whittaker ratio W'_{k,m}(z) / W{k,m}(z) = (1/2 - (m-1/2)/z) - 1/sqrt(z) * W_{k+1/2,m-1/2}(z)/W_{k,m}(z)
 * 
 * Input parameters : 
 * -----------------
 * k = k_x + i * k_y
 * m = m_x + i * m_y
 * z = z_x + i * z_y
 *
 */

void compute_log_w_prime(const double k_x, const double k_y, 
                        const double m_x, const double m_y,
                        const double z_x, const double z_y, 
                        double * res_x, double *res_y, const int use_formula)
{
    slong prec;
    acb_t res, num, denom, temp, z, half, mu, k;
    arb_t tmp1_x, tmp1_y;
    arf_t tmp2_x, tmp2_y;

    acb_init(res);
    acb_init(denom);
    acb_init(num);
    acb_init(k);
    acb_init(z);
    acb_init(temp);
    acb_init(half);
    acb_init(mu);

    arb_init(tmp1_x);
    arb_init(tmp1_y);

    arf_init(tmp2_x);
    arf_init(tmp2_y);
    
    acb_set_d_d(half, 0.5, 0);
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(mu, m_x, m_y);
    acb_set_d_d(k, k_x, k_y);

    for (prec = PREC_MIN; prec <= PREC_MAX; prec *= 2 )
    {
        whittaker_w_with_prec(k_x, k_y, m_x, m_y, z_x, z_y, &denom, prec, 0);
       
        if (use_formula == 2) // (1/2 - k/z) - 1/z * W_{k+1,m}(z)/W_{k,m}(z)
 
        {
            whittaker_w_with_prec(k_x + 1, k_y, m_x, m_y, z_x, z_y, &num, prec, 0);

            acb_div(res, num, denom, prec);

            acb_inv(temp, z, prec);
            acb_mul(res, res, temp, prec);

 
            acb_div(temp, k, z, prec);  
            acb_sub(temp, half, temp, prec); 
            acb_sub(res, temp, res, prec);
        }
        else // (1/2 - (m-1/2)/z) - 1/sqrt(z) * W_{k+1/2,m-1/2}(z)/W_{k,m}(z)
 
        {
            whittaker_w_with_prec(k_x + 0.5, k_y, m_x - 0.5, m_y, z_x, z_y, &num, prec, 0);

            acb_div(res, num, denom, prec);

            acb_sqrt(temp, z, prec);
            acb_inv(temp, temp, prec);
            acb_mul(res, res, temp, prec); 
         
            acb_set_d_d(mu, m_x, m_y);
            acb_sub(mu, mu, half, prec);  
            acb_div(mu, mu, z, prec); 
            acb_sub(mu, half, mu, prec);
            acb_sub(res, mu, res, prec);

        }
        if (acb_is_finite(res) && acb_rel_accuracy_bits(res) >= 53)
            break;
    }
    if (prec > PREC_MAX)
    {
        printf("WARNING PREC_MAX is reached, the result is probably false");
    }

    /* retrieve real and imaginary parts of the result */
    acb_get_real(tmp1_x, res);
    acb_get_imag(tmp1_y, res);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp2_x, tmp1_x, prec);
    arb_get_ubound_arf(tmp2_y, tmp1_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp2_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp2_y, ARF_RND_NEAR);

    /* clean */
    acb_clear(res);
    acb_clear(z);
    acb_clear(temp);
    acb_clear(half);
    acb_clear(mu);
    
    arb_clear(tmp1_x);
    arb_clear(tmp1_y);
    
    arf_clear(tmp2_x);
    arf_clear(tmp2_y);
    flint_cleanup();

}

void compute_spherical_bessel_j(const int n,  const double z_x, const double z_y, 
                                double * res_x,  double * res_y)
{

    slong prec = 64;
    acb_t nu, z, u, v, j;
    arb_t half, j_x, j_y;
    arf_t tmp_x, tmp_y;

    /* initialisation */
    acb_init(nu);
    acb_init(z);
    acb_init(j);
    acb_init(u);
    acb_init(v);

    arb_init(half);
    arb_init(j_x);
    arb_init(j_y);

    arf_init(tmp_x);
    arf_init(tmp_y);

    /* z <- z_x + i * z_y
     * u <- n + i * 0
     * half = 1/2
     *                     */
    acb_set_d_d(z, z_x, z_y);
    acb_set_d_d(u, (slong) n, 0);
    arb_set_d(half, 0.5);

    /* nu = u + 1/2 */
    acb_add_arb(nu, u, half, prec);  

    /* u = pi */
    acb_const_pi(u, prec);

    /* v = pi/2 */
    acb_mul_arb(v, u, half, prec);

    /* u =  v / z */
    acb_div(u, v, z, prec);

    /* v = sqrt(u) */
    acb_sqrt(v, u, prec);

    /* u = bessel_j(n+1/2, z) */
    acb_hypgeom_bessel_j(u, nu, z, prec);

    /* res = sqrt(...) * bessel(n+1/2, z) */
    acb_mul(j, v, u, prec);

    /* retrieve real and imaginary parts of the result */
    acb_get_real(j_x, j);
    acb_get_imag(j_y, j);

    /* pass from real balls to arbitrary-precision floating-point numbers */
    arb_get_ubound_arf(tmp_x, j_x, prec);
    arb_get_ubound_arf(tmp_y, j_y, prec);

    /* rounding to double */
    *res_x = arf_get_d(tmp_x, ARF_RND_NEAR);
    *res_y = arf_get_d(tmp_y, ARF_RND_NEAR);


    /* cleaning */  
    acb_clear(nu); 
    acb_clear(z);
    acb_clear(u);
    acb_clear(v);
    acb_clear(j);
    
    arb_clear(j_x);
    arb_clear(j_y);
    arb_clear(half);

    arf_clear(tmp_x);
    arf_clear(tmp_y);

}
