testARB
-------
this project aims to provides Fortran interfaces for special functions implemented through the ARB[https://github.com/fredrik-johansson/arb] library
Currently, we have interfaces for 
- the W Whittaker function 
```math
exp(-z/2) z^{m+1/2} U(1/2+m -k, 1+2m, z)
```
- the M Whittaker function 
```math
exp(-z/2) z^{m+1/2} \frac{_1F_1(1/2+m -k, 1+2m, z)}{\Gamma(1+2m)}
```
- the J-Bessel function with complex arguments

- the Y-Bessel function with complex arguments
```math
Y_{\nu}(z) = \frac{\cos(\nu \pi) J_{\nu}(z) - J_{-\nu}(z)}{\sin(\nu \pi)}
```
- the m-th derivative of Legendre polynomial of order n (normalized) 
```math
P_n^m(z) = \frac{(1+z)^{m/2}}{(1-z)^{m/2}}
    \mathbf{F}\left(-n, n+1, 1-m, \frac{1-z}{2}\right)
```
- the spherical harmonic Y(Θ,Φ)
```math
Y_n^m(\theta, \phi) = \sqrt{\frac{2n+1}{4\pi} \frac{(n-m)!}{(n+m)!}} e^{im\phi} P_n^m(\cos(\theta)).
```
- a more robust (using arb compared to the intrinsic)  version of the Γ function
Prerequisites:
--------------
 - install flint (check in your distribution)
 - install ARB 
```   
git clone https://github.com/fredrik-johansson/arb && cd arb && ./configure --prefix=/path/to/install && make && make install
```
INSTALL
-------
 - clone the repository
```
git clone git@gitlab.inria.fr:fuentes/testarb.git
```
 - go to subdirectory 
```
cd testarb
```
 - configure, compile  and test
```
mkdir build && cd build && cmake -DARB_PREFIX=/path/to/install .. && make && make test
```
