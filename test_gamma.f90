module test_gamma

    use iso_c_binding
    use m_asserts
    use m_hypergeom
    implicit none

    real(c_double), parameter :: prec = real(1.e-12, c_double)
    complex(c_double) :: res, res_check

contains 

    subroutine test_gamma_simple
        res_check = cmplx(-0.0134162030007208d0, - 0.0126214650579304d0, c_double)
        res = robust_gamma(cmplx( 10.d0, -22.d0, c_double))
        call assert_equals_c( res, res_check, prec )
    end subroutine test_gamma_simple
end module test_gamma

