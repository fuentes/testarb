from flint import *


def whittaker_w(k, m, z):
    a = 1/2. + m - k
    b =  1 + 2. * m
    #return acb.exp(-z/2.)*acb.pow(z, m+1/2.)*
    #return acb.hypgeom_u(a, b, z, n = 12,asymp=True)
    return acb.exp(-z/2.)*acb.pow(z, m+1/2.)*acb.hypgeom_u(a, b, z)

def whittaker_m(k, m, z):
    a = 1/2. + m - k
    b =  1 + 2. * m
    return acb.exp(-z/2.)*acb.pow(z, m+1/2.)*acb.hypgeom_1f1(a, b, z)


k = acb(2,-1)
m = acb(100.5, 0.)
z = acb(200, 150)
#showgood(lambda: whittaker_w(k, m, z), dps=25)
showgood(lambda: whittaker_m(k, m , z), dps=25)
