module test_whittakers

    use iso_c_binding
    use m_asserts
    use m_hypergeom
    implicit none

    real(c_double), parameter :: pi = 4.d0*atan(1.d0), prec = real(1.e-12, c_double)
    real(c_double) :: a_x, res_x_check, res_y_check, res_x, res_y
    complex(c_double) :: res, a, k, m , res_check
contains
     
    subroutine test_compute_u
        res_x_check = 0.416940305395154d0; ! taken from sage hypergeometric_U(12,20,3.) 
        call compute_u( 12.d0, 20.d0, 3.d0, 0.d0, res_x, res_y )
        call assert_equals_d( res_x, res_x_check, prec )
    end subroutine test_compute_u
    
    subroutine test_compute_u_complex
       res_x_check = -0.00646612339420241d0 
       res_y_check  = 0.0112751987320501d0;
       call compute_u(12.d0, 20.d0, 3.d0, 2.d0, res_x, res_y); ! taken from hypergeometric_U(12, 20, 3.+2.*I)
       call assert_equals_d( hypot(res_x - res_x_check, res_y - res_y_check) , 0.d0,  prec);
    end subroutine test_compute_u_complex

    subroutine test_whittaker_real 
        a_x = 1.3d0
        a = cmplx( a_x, 0.d0, c_double)
        k = cmplx(-.25d0, 0.d0, c_double) 
        res = whittaker_w( k , -k, a * a )
        call assert_equals_d( erfc(a_x), exp( -a_x * a_x / 2.d0) / sqrt(pi * a_x) * real( res, c_double), prec )
    end subroutine test_whittaker_real 

    subroutine test_whittaker_w
        k = cmplx( 0.d0, -1.d0, c_double)
        m = cmplx(200.5d0, 0.d0, c_double)
        a = cmplx(0.d0, -500d0, c_double)
        res_check = cmplx(-0.269882160121987d0, -0.0179493521772896d0, c_double)
        res = whittaker_w( k , m, a)
        call assert_equals_c( res, res_check, prec )
    end subroutine test_whittaker_w

    subroutine test_log_w_prime
        k = cmplx(1.1474488447893996d0,2.39352191674488477d-4, c_double)
        m = cmplx(0.5d0, 0.d0, c_double)
        a = cmplx(6065.8287682093933d0,-1.2653020799890684d0, c_double)
        res_check = cmplx(-0.499810829371811d0, 7.89285550251151d-8, c_double)
        res = log_w_prime( k , m, a)
        call assert_equals_c( res, res_check, prec )
    end subroutine test_log_w_prime

    subroutine test_whittaker_M
        k = cmplx( 0.d0, -1.d0, c_double)
        m = cmplx(7.2d0, 0.d0, c_double)
        a = cmplx(0.d0, -50d0, c_double)
        res_check = cmplx(0.00301625449482876d0, 0.00153685842726721d0, c_double)
        res = whittaker_m( k , m, a)
        call assert_equals_c( res, res_check, prec )
    end subroutine test_whittaker_M

end module test_whittakers
