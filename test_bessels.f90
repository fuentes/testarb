module test_bessels

    use iso_c_binding
    use m_hypergeom
    use m_asserts
    implicit none
    
    real(c_double), parameter :: prec = real(1.e-12, c_double)
    complex(c_double) :: res, res_check, nu, z, theta, phi
    integer(c_int) :: n
contains   

    
    subroutine  test_bessel_j 
        nu = dcmplx( 1.d0, -2.d0 )
        z = dcmplx( 2.d0, 3.d0 )
        res_check = dcmplx( 76.5569916530948d0,  56.1590212932692d0 )
        res = bessel_j( nu, z )
        call assert_equals_c( res , res_check,  prec )
    end subroutine test_bessel_j

    subroutine test_bessel_y
        res_check = dcmplx( -56.1598484095213d0, 76.5569180178113d0 )
        res = bessel_y( nu, z )
        call assert_equals_c( res , res_check,  prec )
    end subroutine test_bessel_y

    subroutine test_spherical_bessel_j
        res_check = dcmplx(  2193.21745322709d0, - 167.355288750555d0)
        res = spherical_bessel_j( 25, dcmplx(20.d0, 20.3d0))
        call assert_equals_c( res , res_check,  10*prec ) ! prec is 3.64e-12
    end subroutine test_spherical_bessel_j
    
end module test_bessels
