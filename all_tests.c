#include <stdio.h>
#include <math.h>
#include <assert.h>

/* external declarations */
extern void compute_U(const double m, const double n, const double z_x, const double z_y, double * res_x, double *res_y);

extern void compute_whittaker_w(const double k_x, const double k_y, const double m_x, const double m_y, const double z_x, const double z_y, 
                              double * res_x, double *res_y, const int asymp);


extern void compute_log_w_prime(const double k_x, const double k_y, const double m_x, const double m_y, const double z_x, const double z_y, 
                              double * res_x, double *res_y, const int use_formula);

extern void compute_whittaker_m(const double k_x, const double k_y, const double m_x, const double m_y, const double z_x, const double z_y, 
                              double * res_x, double *res_y);
extern void compute_bessel_j(const double nu_x, const double nu_y, const double z_x, const double z_y, double * res_x, double *res_y);

extern void compute_bessel_y(const double nu_x, const double nu_y, const double z_x, const double z_y, double * res_x, double *res_y);


extern void compute_spherical_bessel_j(const int n,  const double z_x, const double z_y, 
                                                     double * res_x,  double * res_y);

extern void compute_gamma(const double z_x, const double z_y, double *res_x, double *res_y);


extern void compute_spherical_y(const int m, const int n, 
                                const double theta_x, const double theta_y,
                                const double phi_x, const double phi_y,
                                double *res_x, double *res_y);

extern void compute_legendre_p(const double m_x, const double m_y,
                               const double n_x, const double n_y,
                               const double z_x, const double z_y,
                               double *res_x, double *res_y);

extern void compute_legendre_p_rec(const int n_i, const double r_d, double *res_d);


extern void compute_v_dot_P( const int n_v, const double * v_x, const double * v_y, const double theta, double * res_x, double * res_y);


int main()
{
  double prec = 1e-12;
  double res_x_check, res_y_check;
  double res_x, res_y;
  double err;
  double a;
  /* first test : real case */
  res_x_check = 0.416940305395154; // taken from sage hypergeometric_U(12,20,3.) 
  compute_U(12, 20, 3., 0., &res_x, &res_y);
  printf("|U(12,20,3) - 0.416940305395154 | = %e\n",fabs(res_x-res_x_check));
  assert(fabs(res_x-res_x_check) < prec);
  /* second case : complex case */
  res_x_check = -0.00646612339420241; 
  res_y_check  = 0.0112751987320501;
  compute_U(12, 20, 3., 2., &res_x, &res_y); //taken from hypergeometric_U(12, 20, 3.+2.*I)
  printf("|U(12, 20, 3 + 2i) - (-0.00646612339420241 + 0.0112751987320501 * i) |  = %e\n",hypot(res_x-res_x_check, res_y-res_y_check));
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);
  /* test1 for whittaker */
  a = 1.3;
  compute_whittaker_w( -.25, 0., 0.25, 0., a * a  , 0., &res_x, &res_y, 0);
  err = fabs(erfc(a) - exp( -a * a / 2.) / sqrt(M_PI * a) * res_x);
  printf("|erfc(x) - exp(-a*a/2) /sqrt(a*pi) * U(-1/4,1/4, a*a)| = %e\n", err);
  fflush(NULL);
  assert(err < prec);

  /* test with bigger values */
  compute_whittaker_w( 0, -1., 200.5, 0, 0., -500, &res_x, & res_y, 0);
  res_x_check = -0.269882160121987;
  res_y_check = -0.0179493521772896;
  printf("| %e + %e * I\n", res_x, res_y);
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test (log(w))' */
  compute_log_w_prime( 1.1474488447893996,2.39352191674488477E-004, 0.500000000000000, 0., 6065.8287682093933,-1.2653020799890684, &res_x, & res_y, 2);
  res_x_check = -0.499810829371811 ; 
  res_y_check =  7.89285550251151e-8;
  printf("log(w)' = | %e + %e * I\n", res_x, res_y);
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test 2 for (log(w))' */
  compute_log_w_prime( -3.4285932461439130,-0.65426914382547996, 500.50000000000000, 0., 1866.7408832728738,-356.22509634719677, &res_x , &res_y, 1);
  res_x_check = -0.499810829371811 ; 
  res_y_check =  7.89285550251151e-8;
  printf("log(w) test 2' = | %e + %e * I\n", res_x, res_y);
  //assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);


/* whittaker_M with same values */
  compute_whittaker_m( 0, -1., 7.2, 0, 0., -50., &res_x, & res_y);
  res_x_check = 0.00301625449482876;
  res_y_check = 0.00153685842726721;
  printf("| %e + %e * I\n", res_x, res_y);
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test bessel_j */

  res_x_check = 76.5569916530948;
  res_y_check = 56.1590212932692;

  compute_bessel_j(1., -2., 2., 3., &res_x, &res_y);

  printf("| %e + %e * I\n", res_x, res_y);
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test bessel_y */
  res_x_check = -56.1598484095213;
  res_y_check =  76.5569180178113;

  compute_bessel_y(1., -2., 2., 3., &res_x, &res_y);

  printf("| %e + %e * I\n", res_x, res_y);
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test spherical_bessel_j */
  compute_spherical_bessel_j(25, 20, 20.3, &res_x, &res_y);
  res_x_check = 2193.21745322709;
  res_y_check = - 167.355288750555;

  printf("| %e + %e * I\n", res_x, res_y);
  printf("| error spherical bessel_j = %e \n", hypot(res_x-res_x_check, res_y-res_y_check));
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < 5*prec);

  /*  test gamma */

  compute_gamma(10.,-22., &res_x, &res_y);
  res_x_check = -0.0134162030007208;
  res_y_check = - 0.0126214650579304;

  printf("| %e + %e * I\n", res_x, res_y);
  printf("| error gamma = %e \n", hypot(res_x-res_x_check, res_y-res_y_check));
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < 5*prec);


  /*  test spherical_y */

  compute_spherical_y(50, 3, 1, 0., 2, 0, &res_x, &res_y);
  res_x_check = -0.158526311974223; 
  res_y_check = 0.0461321382818893;

  printf("| %e + %e * I\n", res_x, res_y);
  printf("| error spherical_y = %e \n", hypot(res_x-res_x_check, res_y-res_y_check));
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test legendre_p */

  compute_legendre_p(10, 0, 4, 0, 1., 2., &res_x, &res_y);
  res_x_check =  1.84792608000000e9; 
  res_y_check = -3.55314960000000e9;
  
  printf("| %e + %e * I\n", res_x, res_y);
  printf("| error legendre_p = %e \n", hypot(res_x-res_x_check, res_y-res_y_check));
  assert(hypot(res_x-res_x_check, res_y-res_y_check) < prec);

  /* test legendre_p_rec */

  compute_legendre_p_rec(25, 0.57, &res_x);
  res_x_check = 0.0412029868641242; 
  
  printf("%e \n", res_x);
  printf("| error legendre_p_rec = %e \n", fabs(res_x -res_x_check));
  assert(fabs(res_x - res_x_check) < prec);

  /* test  compute_legendre_p_rec*/
  { double v_x[] = {1.1578202807271805, -0.05259817580430567, -3.0438470369992775e-05, 1.8333345105266042e-05, -2.1154860578343941e-07, 8.696361041685358e-10, -5.063829767457681e-13, -7.536878726934683e-15, 2.7952124548363804e-17, -4.258088456568249e-20};
  double v_y[] = {-0.1695696988566226, 0.05985194868316748, -0.002024102751901475, 1.9211818941768738e-05, -1.2021933707160461e-08, -7.29520445392943e-10, 4.331908711753989e-12, -1.0098923045231365e-14, 4.839247924100385e-18, 2.834634322147961e-20};
  double theta = acos(0.5*sqrt(2));
  double k_x =  0.3;
  double k_y = 0.75;
  
  double k_mul_res_x;
  double k_mul_res_y;
  compute_v_dot_P( 9, v_x, v_y, theta, &res_x, &res_y);
  res_x_check = 0.0276709663803235;
  res_y_check = 0.0613613553419072;
 
  k_mul_res_x = (res_x * k_x - res_y * k_y) / (4.0 * M_PI);
  k_mul_res_y = (res_x * k_y + res_y * k_x) / (4.0 * M_PI);

  printf("%e %e\n", k_mul_res_x, k_mul_res_y);
  printf("| error compute_v_dot_p = %e \n", fabs(k_mul_res_x -res_x_check) + fabs(k_mul_res_y - res_y_check));
  assert(fabs(k_mul_res_x - res_x_check) + fabs(k_mul_res_y - res_y_check)  < prec);
  }

  return 0;
}
