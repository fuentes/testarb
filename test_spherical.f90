module test_spherical

    use iso_c_binding
    use m_asserts
    use m_hypergeom
    implicit none

    real(c_double), parameter :: prec = real(1.e-12, c_double)
    complex(c_double) :: res,  res_check
  
contains 

    subroutine test_spherical_y 
        res_check = cmplx(-0.158526311974223d0, 0.0461321382818893d0, c_double)
        res = spherical_y(50, 3, dcmplx(1.d0, 0.d0), dcmplx(2.0, 0.d0))
        call assert_equals_c( res, res_check ,  prec)
    end subroutine test_spherical_y 

end module test_spherical

